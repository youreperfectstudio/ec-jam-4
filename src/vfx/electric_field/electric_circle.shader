shader_type canvas_item;
render_mode blend_mix;

uniform vec4 color : hint_color = vec4(0.3, 0.3, 1.0, 1.0);

uniform float border_radius : hint_range(0.0, 1.0) = 0.1;

void fragment() {
	vec2 uv = UV * 2.0 - 1.0;

	if (length(uv) > 1.0 - border_radius)
		discard;

	COLOR = vec4(color.rgb, 1.0);
	//COLOR = vec4(0.0, 1.0, 0.0, 1.0);
}

