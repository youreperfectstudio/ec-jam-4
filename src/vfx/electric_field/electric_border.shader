shader_type canvas_item;
render_mode blend_mix;

uniform vec4 border_color : hint_color = vec4(0.6, 0.6, 1.0, 1.0);

uniform sampler2D filled_buffer : hint_white;

void fragment() {
	vec2 uv = UV * 2.0 - 1.0;
	if (length(uv) > 1.0)
		discard;

	vec4 prev_value = texture(filled_buffer, SCREEN_UV);

	float was_filled = float(prev_value.a > 0.5);
	COLOR = mix(border_color, prev_value, was_filled);
}

