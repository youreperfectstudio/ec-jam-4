extends MeshInstance2D

func _ready():
	var size = get_viewport_rect().size
	mesh.size = size
	position = size / 2.0
