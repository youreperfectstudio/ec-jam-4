extends Node2D

onready var border_viewport = get_node('BorderViewport')
onready var filled_viewport = get_node('FilledViewport')

export(PackedScene) var electric_border
export(PackedScene) var electric_circle
export(Material) var electric_border_material

var fields_data = {}
var camera_position setget set_camera_position

func force_clear():
	for node in border_viewport.get_node('Root').get_children():
		node.queue_free()

	for node in filled_viewport.get_node('Root').get_children():
		node.queue_free()

func set_camera_position(value):
	camera_position = value
	$'ViewportQuad'.position = camera_position
	border_viewport.get_node('Root').position = -camera_position + get_viewport_rect().size / 2
	filled_viewport.get_node('Root').position = -camera_position + get_viewport_rect().size / 2

func _ready():
	var size = get_viewport_rect().size
	border_viewport.size = size
	filled_viewport.size = size

func add_electric_field(node):
	var electric_border_inst = electric_border.instance()
	electric_border_inst.position = node.position
	electric_border_inst.overwrite_material(electric_border_material)
	electric_border_inst.visible = node.powered
	border_viewport.get_node('Root').add_child(electric_border_inst)

	var electric_field_inst = electric_circle.instance()
	electric_field_inst.position = node.position
	electric_field_inst.visible = node.powered
	filled_viewport.get_node('Root').add_child(electric_field_inst)

	fields_data[node] = [electric_border_inst, electric_field_inst]

	node.connect('moved', self, 'on_node_moved', [node])
	node.connect('about_to_be_deleted', self, 'remove_node', [node])
	node.connect('power_changed', self, 'on_power_changed', [node])

func on_node_moved(pos, node):
	for data in fields_data[node]:
		data.position = pos

func remove_node(node):
	if not fields_data.has(node):
		return

	for data in fields_data[node]:
		data.queue_free()
		fields_data.erase(node)

func on_power_changed(powered, node):
	if not fields_data.has(node):
		return

	for data in fields_data[node]:
		data.visible = powered
