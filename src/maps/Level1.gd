extends Map

func _ready():
	._ready()
	$Portal_Right.connect("portal_dead", self, "reduce_portal_amt" ) 
	$Portal2_Left.connect("portal_dead", self, "reduce_portal_amt" ) 
	$Portal3_Middle.connect("portal_dead", self, "reduce_portal_amt" ) 

func _on_Bunker_about_to_be_deleted():
	on_bunker_destroyed()
