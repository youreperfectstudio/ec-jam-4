extends Node2D
class_name Map

const STARTING_CURRENCY = 4000
const NODE_COST = 100
const TOWER_COST = 500

var node = preload("res://images/ElectricalNodeUnlit_Icon.png")
var tower = preload("res://images/Tower_Online_Icon.png")
var node_prefab = preload("res://buildings/ElectricNode.tscn")
var tower_prefab = preload("res://buildings/Tower.tscn")
var last_cursor = null
var mouse_up = true
var currency = STARTING_CURRENCY
var score = 0
var score_timer = 0
var over_ui:bool = false
var paused:bool = false
### Future versions should not hard code portal amounts Bad Bad programmer... slaps own hands!!
var portal_amount = 3
var mouse_over_count:int = 0

onready var power_node_manager = get_node('PowerNodeManager')

func _ready():
	power_node_manager.connect("power_node_initialized", self, "on_power_node_added")
	show_inst()

func hide_inst():
	$CanvasLayer/InstructionsBox.hide()

func show_inst():
	$CanvasLayer/InstructionsBox.show()
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	Input.set_custom_mouse_cursor(null,0,Vector2.ZERO)
	last_cursor = null

func reduce_portal_amt():
	portal_amount -= 1
	if portal_amount <=0:
		on_portals_destroyed()
		
func on_power_node_added(node):
	node.connect("about_to_be_deleted", self, "on_building_destroyed")
	node.connect("about_to_be_deleted",self,"on_building_destroyed")
	node.connect("mouse_enter",self,"on_building_mouse_entered")
	node.connect("mouse_leave",self,"on_building_mouse_exited")

func _input(ev):
	if (ev is InputEventScreenTouch):
		if ev.pressed:
			$CanvasLayer/SettingsBox/AutoScroll.pressed = false

func _process(delta):
	if $CanvasLayer/SettingsBox/AutoScroll.pressed:
		$Camera2D.mouse_scroll = true
		$CanvasLayer/ScrollButtons.hide()
	else:
		$Camera2D.mouse_scroll = false
		$CanvasLayer/ScrollButtons.show()
	if paused:
		get_tree().paused = true
		if Input.is_action_just_pressed("ui_cancel") && !$CanvasLayer/InstructionsBox.visible:
			_on_SettingsCloseButton_pressed()
	else:
		get_tree().paused = false
		if score_timer >= 60:
			score -= 10
			currency += 1
			score_timer = 0
		score_timer += 1
		update_currency()
		if Input.is_mouse_button_pressed(BUTTON_LEFT):
			if over_ui: return
			if mouse_up:
				match last_cursor:
					node:
						if currency >= NODE_COST:
							power_node_manager.add_power_node(get_global_mouse_position())
							currency -= NODE_COST
							on_building_mouse_entered()
					tower:
						if currency >= TOWER_COST:
							var new_node = tower_prefab.instance()
							new_node.connect("about_to_be_deleted",self,"on_building_destroyed")
							new_node.connect("mouse_enter",self,"on_building_mouse_entered")
							new_node.connect("mouse_leave",self,"on_building_mouse_exited")
							$Towers.add_child(new_node)
							new_node.set_global_position(get_global_mouse_position())
							currency -= TOWER_COST
					null:
						pass
				mouse_up = false
		else:
			mouse_up = true
		if Input.is_action_just_pressed("ui_cancel"):
			_on_SettingsButton_pressed()

func update_currency():
	if currency < 0: currency = 0
	if currency > 999999999: currency = 999999999
	if score < 0: score = 0
	if score > 999999999: score = 999999999
	$CanvasLayer/ResourcesBox/Currency.text = "$ " + String(currency)
	$CanvasLayer/ScoreBox/Score.text = String(score)

func on_enemy_destroyed():
	currency += 30
	score += 100

func on_building_destroyed():
	score -= 200

func on_bunker_destroyed():
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	Input.set_custom_mouse_cursor(null,0,Vector2.ZERO)
	over_ui = true
	$CanvasLayer/GameOver/FinalScore.text = String(score)
	$CanvasLayer/GameOver/GameOver.text = "Game Over"
	$CanvasLayer/GameOver.show()

func on_portals_destroyed():
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	Input.set_custom_mouse_cursor(null,0,Vector2.ZERO)
	over_ui = true
	$CanvasLayer/GameOver/GameOver.text = "You Won!!"
	$CanvasLayer/GameOver/FinalScore.text = String(score)
	$CanvasLayer/GameOver.show()


func _on_Button_pressed(building_name:String):
	if building_name == "Node":
		Input.set_custom_mouse_cursor(node,0,Vector2(16,16))
		last_cursor = node
	else:
		Input.set_custom_mouse_cursor(tower,0,Vector2(32,32))
		last_cursor = tower

#not working for some reason - oh well
func _on_ColorRect_focus_entered():
	Input.set_custom_mouse_cursor(null,0,Vector2.ZERO)
	over_ui = true
	mouse_over_count = 0


func _on_ColorRect_focus_exited():
	over_ui = false
	match last_cursor:
		node:
			Input.set_custom_mouse_cursor(node,0,Vector2(16,16))
		tower:
			Input.set_custom_mouse_cursor(tower,0,Vector2(32,32))
		null:
			pass


func _on_Rocks_mouse_entered():
	mouse_over_count += 1
	if last_cursor != null:
		_on_ColorRect_focus_entered()
		Input.set_default_cursor_shape(Input.CURSOR_FORBIDDEN)


func _on_Rocks_mouse_exited():
	mouse_over_count -= 1
	if mouse_over_count < 1:
		mouse_over_count = 0
		Input.set_default_cursor_shape(Input.CURSOR_ARROW)
		_on_ColorRect_focus_exited()

func on_building_mouse_entered():
	_on_Rocks_mouse_entered()

func on_building_mouse_exited():
	_on_Rocks_mouse_exited()

func _on_Quit_pressed():
	get_tree().quit()


func _on_Restart_pressed():
	OffscreenRendering.force_clear()
	get_tree().reload_current_scene()


func _on_SettingsCloseButton_pressed():
	$CanvasLayer/SettingsBox.hide()
	paused = false
	over_ui = false


func _on_SettingsButton_pressed():
	$CanvasLayer/SettingsBox.show()
	paused = true
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	Input.set_custom_mouse_cursor(null,0,Vector2.ZERO)
	over_ui = true
	last_cursor = null


func _on_CheatButton_pressed():
	currency += 143143143
