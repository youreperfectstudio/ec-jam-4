extends Control

var last:Vector2 = Vector2.ZERO
var current:Vector2 = Vector2.ZERO

func direction_to_button(direction:Vector2) -> String:
	match direction:
		Vector2.UP:
			return "ui_up"
		Vector2.DOWN:
			return "ui_down"
		Vector2.LEFT:
			return "ui_left"
		Vector2.RIGHT:
			return "ui_right"
		_:
			return ""

func send_event(direction:Vector2,pressed:bool):
		var ev:InputEventAction = InputEventAction.new()
		ev.action = direction_to_button(direction)
		ev.pressed = pressed
		Input.parse_input_event(ev)

func _process(delta):
	if last != Vector2.ZERO:
		send_event(last,false)
	if current != Vector2.ZERO:
		send_event(current,true)

func _on_ScrollUpButton_button_down():
	last = current
	current = Vector2.UP

func _on_ScrollUpButton_button_up():
	last = current
	current = Vector2.ZERO

func _on_ScrollDownButton_button_down():
	last = current
	current = Vector2.DOWN

func _on_ScrollDownButton_button_up():
	last = current
	current = Vector2.ZERO

func _on_ScrollLeftButton_button_down():
	last = current
	current = Vector2.LEFT

func _on_ScrollLeftButton_button_up():
	last = current
	current = Vector2.ZERO

func _on_ScrollRightButton_button_down():
	last = current
	current = Vector2.RIGHT

func _on_ScrollRightButton_button_up():
	last = current
	current = Vector2.ZERO


func _on_ScrollButton_mouse_entered():
	$"../.."._on_ColorRect_focus_entered()


func _on_ScrollButton_mouse_exited():
	$"../.."._on_ColorRect_focus_exited()
