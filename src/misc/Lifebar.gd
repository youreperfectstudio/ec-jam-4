extends Node2D

func _ready():
	$ProgressBar.value = 100

func change_value(health, max_health):
	$ProgressBar.value = (float(health)/max_health) * 100