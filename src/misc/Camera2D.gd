extends Camera2D

export(int) var default_speed = 40
export(int) var margins = 40
export(bool) var mouse_scroll = false

var mx = 0
var my = 0

var drag_camera_start = Vector2.ZERO
var dragging_camera = false

func _ready():
	pass

func _input(event):
	if drag_camera(event):
		return

	check_mouse()

func _process(delta):
	## moves the camera smoothly
	var speed = default_speed * SettingsManager.scroll_coef
	var lerped_x = lerp(position.x, position.x + mx * speed, speed * delta)
	var lerped_y = lerp(position.y, position.y + my * speed, speed * delta)
	if lerped_x < 1920 && lerped_x > -1920:
		position.x = lerped_x
	if lerped_y < 1080 && lerped_y > -1080:
		position.y = lerped_y
	OffscreenRendering.camera_position = get_camera_screen_center()

func drag_camera(event):
	if event is InputEventMouseButton \
			and event.button_index == BUTTON_MIDDLE:
		dragging_camera = event.pressed
		drag_margin_h_enabled = not dragging_camera
		drag_margin_v_enabled = not dragging_camera
		if dragging_camera:
			drag_camera_start = get_global_mouse_position()

	if event is InputEventMouseMotion \
			and dragging_camera:
		position -= get_global_mouse_position() - drag_camera_start

	return dragging_camera

func check_mouse():
	## gets the size of the screen
	var vp_size = get_viewport_rect().size

	#gets the mouse position withing the screen
	var mp = get_viewport().get_mouse_position()

	## sets the movement vectors mx and my, based on the mosition of the mouse
	#if mp.x > (vp_size.x-margins):
	if Input.is_action_pressed("ui_right") || (mouse_scroll && mp.x > (vp_size.x-margins)):
		mx = 1
		# print("moving right")
	#elif mp.x < margins:
	elif Input.is_action_pressed("ui_left") || (mouse_scroll && mp.x < margins):
		mx = -1
		# print("moving left")
	else:
		mx = 0
		# print("not moving")
	#if mp.y > (vp_size.y - margins):
	if Input.is_action_pressed("ui_down") || (mouse_scroll && mp.y > (vp_size.y - margins)):
		my = 1
	#elif mp.y < margins:
	elif Input.is_action_pressed("ui_up") ||  (mouse_scroll && mp.y < margins):
		my = -1
	else:
		my = 0
