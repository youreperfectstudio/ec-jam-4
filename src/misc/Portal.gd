extends Node2D
class_name Portal

signal portal_dead

var type = "portal"
export var health = 2000
export var timeout = 45
export var enemies_to_spawn = 0
onready var enemies = get_node("../Enemies")
onready var map = get_node("..")

var counter = 0
func _ready():
	$Timer.connect("timeout", self, "start_spawning")
	$SpawnDelay.connect("timeout", self, "spawn")
	pass

func start_spawning():
	$SpawnDelay.start()
	enemies_to_spawn += 2
	
func spawn():
	var enemy = preload("res://enemies/Enemy.tscn").instance()
	enemy.connect("destroyed",map,"on_enemy_destroyed")
	enemy.position = position
	enemies.add_child(enemy)
	$AudioStreamPlayer2D.play()
	counter += 1 
	if counter < enemies_to_spawn:
		$SpawnDelay.start()
	else:
		enemies_to_spawn += 1
		counter = 0
		$Timer.start()

func take_damage(amt):
	health -= amt
	if health <= 0:
		death()
		
func death():
	emit_signal("portal_dead")
	queue_free()
	