extends Node2D

export(PackedScene) var power_node_scene

signal power_node_initialized(power_node)

onready var root_node

func _ready():
	root_node = get_child(0)
	root_node.turn_on()

func add_power_node(pos):
	var power_node = power_node_scene.instance()
	power_node.position = pos
	power_node.turn_off()
	power_node.connect('about_to_be_deleted', self, 'on_node_death', [power_node])

	add_child(power_node)

	# Wait next physics frame
	yield(get_tree(), 'physics_frame')
	yield(get_tree(), 'physics_frame')
	power_node.connect_to_grid()

	emit_signal('power_node_initialized', power_node)

func on_node_death(dead_node):
	dead_node.remove_connections()

	# Reinit connected node tree
	var new_nodes_states = {}
	for child in get_children():
		new_nodes_states[child] = false

	new_nodes_states[root_node] = true
	var iterate_queue = [root_node]
	while not iterate_queue.empty():
		var cur_node = iterate_queue.pop_front()
		for connected_node in cur_node.connections:
			if new_nodes_states[connected_node]:
				continue
			new_nodes_states[connected_node] = true
			iterate_queue.push_back(connected_node)

	# Set state after detachment
	new_nodes_states.erase(dead_node)
	for node in new_nodes_states:
		node.neighbours.erase(dead_node)
		if node.powered and new_nodes_states[node]:
			continue
		node.set_powered(new_nodes_states[node])

	# Reconnect detached nodes if possible
	for node in new_nodes_states:
		if not node.powered:
			continue

		for neighbour in node.neighbours:
			if not neighbour.powered:
				neighbour.connect_to_grid()

# Debug Code
#func _input(event):
#	if event is InputEventMouseButton \
#			and event.button_index == BUTTON_LEFT \
#			and event.is_pressed():
#		add_power_node(get_global_mouse_position())

