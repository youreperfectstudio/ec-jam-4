extends Node

signal music_volume_changed(value)
signal sfx_volume_changed(value)

signal scroll_coef_changed(value)

var music_volume = 1.0 setget set_music_volume

func set_music_volume(value):
	music_volume = value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Music'), linear2db(music_volume))
	emit_signal('music_volume_changed', music_volume)

var sfx_volume = 1.0 setget set_sfx_volume

func set_sfx_volume(value):
	sfx_volume = value
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index('SFX'), linear2db(sfx_volume))
	emit_signal('sfx_volume_changed', sfx_volume)

var scroll_coef = 1.0 setget set_scroll_coef

func set_scroll_coef(value):
	scroll_coef = value
	emit_signal('scroll_coef_changed', scroll_coef)

func _ready():
	# TODO: Read settings from file
	pass 

func save_settings():
	# TODO: Save settings to file
	pass
