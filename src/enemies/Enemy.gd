extends KinematicBody2D
class_name Enemy

var type = "enemy"

export var health = 50
export var damage = 20
export var speed = 2000
export var damage_radius = 50
export (int) var attack_cooldown = 1
export (Vector2) var bunker_pos

var bunker_path = null
var current_target = null
var path = null
var target = null
var max_health = health
var pathfinding = true

signal destroyed

func _process(delta):
	if has_node(current_target):
		target = get_node(current_target)
		path = $"../../Nav".get_simple_path(position, target.position, false)
		if pathfinding:
			move(path[1], delta)
		else:
			if $Timer.is_stopped():
				$Timer.start()
	else:
		current_target = bunker_path
		pathfinding = true

func _ready():
	bunker_path = get_node("../../PowerNodeManager/Bunker").get_path()
	current_target = bunker_path
	$SensingArea.connect("area_entered", self, "check_area")
	$Timer.connect("timeout", self, "attack")
	$ParticleTimer.connect("timeout", self, "stop_emitting")
	pass

func attack():
	if has_node(current_target) and is_instance_valid(target):
		if $SensingArea.overlaps_area(target.get_node("DamageArea")):
			#print (name, " is attacking ", target.name, "at", target.health)
			if get_node(str(target.get_path(),"/Sprite")).visible == true:
				target.take_damage(damage)
				
	else:
		#if current target does not exist switch to bunker
		current_target = bunker_path
		pathfinding = true

func check_area(area):
	var parent = area.get_parent()
	if parent is Building:
		if target != null && is_instance_valid(target) && !target.is_queued_for_deletion() && has_node(target.get_path()):
			if target is Bunker && not parent is Bunker: 
				current_target = parent.get_path()
		

func move(point, delta):
	#print (target.position, path.size())
	var v  = (point - position).normalized()
	v *= delta * speed
	
	if path.size() > 0:
		#position += v
		move_and_slide(v)
	if position.distance_to(target.position) < 80:
		pathfinding = false
	if name == "Enemy":
		pass
		#print(position.distance_to(target.position))
		
func move_to():
	pass

func stop_emitting():
	$CPUParticles2D.emitting = false

func take_damage(amt):
	$AudioStreamPlayer2D.play()
	$CPUParticles2D.emitting = true
	$ParticleTimer.start()
	if amt >= health:
		death()
	else:
		health -= amt
	
	$Lifebar.change_value(health, max_health)

func death():
	emit_signal("destroyed")
	queue_free()		
