extends Building
class_name Tower

var powered_by = []

func _ready():
	connect('power_changed', self, 'on_power_changed')
	on_power_changed(powered)

func on_power_changed(power):
	if power:
		if $'AttackTimer'.is_stopped():
			$'AttackTimer'.start()
		$CPUParticles2D.emitting = true
	else:
		$'AttackTimer'.stop()
		$CPUParticles2D.emitting = false

func add_power_source(node):
	powered_by.append(node)
	if not powered and node.powered:
		self.powered = true

	node.connect('about_to_be_deleted', self, 'on_power_source_destroyed', [node])
	node.connect('power_changed', self, 'on_power_source_power_changed', [node])

func on_power_source_destroyed(node):
	powered_by.erase(node)
	recheck_power()

func on_power_source_power_changed(power, node):
	recheck_power()

func recheck_power():
	var power = false
	for node in powered_by:
		if node.powered:
			power = true
			break
	self.powered = power