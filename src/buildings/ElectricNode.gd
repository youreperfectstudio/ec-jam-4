extends Building
class_name ElectricNode

export(PackedScene) var power_line_scene

var neighbours = []
var connections = {}

static func closest_sort(a, b):
	if a[1] < b[1]:
		return true
	return false

func _ready():
	._ready()
	OffscreenRendering.add_electric_field(self)
	$'PowerArea'.connect('area_entered', self, 'on_electric_node_entered')
	connect('power_changed', self, 'on_power_changed')
	connect('about_to_be_deleted', self, 'on_death')

func on_power_changed(power):
	if power:
		for neighbour in neighbours:
			if not neighbour.powered:
				neighbour.connect_to_grid()
	else:
		remove_connections()

func remove_connections():
	for connected_node in connections:
		connections[connected_node].queue_free()
		connected_node.connections.erase(self)
		connections.erase(connected_node)

func on_death():
	for connection in connections.values():
		connection.queue_free()

	for neighbour in neighbours:
		neighbour.connections.erase(self)
		neighbour.neighbours.erase(self)

func add_connection(node):
	var power_line = power_line_scene.instance()
	connections[node] = power_line
	node.connections[self] = power_line
	power_line.points = PoolVector2Array([Vector2(0, 0), node.position - position])
	add_child(power_line)

func add_neighbour(node):
	if neighbours.has(node):
		return

	# Electric Node
	if node.get('neighbours') != null:
		neighbours.append(node)
		node.neighbours.append(self)

	# Powered building
	if node.get('powered_by') != null:
		node.add_power_source(self)

func remove_neighbour(node):
	neighbours.erase(node)
	node.neighbours.erase(self)

func on_electric_node_entered(area):
	var node = area.get_parent()
	if node == self:
		return

	add_neighbour(node)

func connect_to_grid():
	if powered:
		return

	# Sort neighbours by distance
	var neighbours_distance = []
	for neighbour in neighbours:
		neighbours_distance.append([neighbour, (position - neighbour.position).length()])

	neighbours_distance.sort_custom(self, 'closest_sort')

	for n in neighbours_distance:
		var neighbour = n[0]
		if neighbour.powered:
			add_connection(neighbour)
			turn_on()
			break

# Debug Code
#func _input(event):
#	if event is InputEventMouseButton \
#			and event.button_index == BUTTON_RIGHT \
#			and event.is_pressed() \
#			and (get_global_mouse_position() - position).length() < 10.0:
#		death()
