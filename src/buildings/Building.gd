extends Node2D
class_name Building

signal moved(new_pos)
signal about_to_be_deleted()
signal power_changed(powered)
signal mouse_enter()
signal mouse_leave()

export var health = 200
export var damage = 20
export var damage_radius = 300
export var power_radius = 50
export (bool) var  can_attack = true
export (int) var attack_cooldown = 1
export var type = "building"
var current_target = null
var bodies = null
var max_health = health
export (Texture) var powered_texture
export (Texture) var unpowered_texture

var powered = false setget set_powered

func _process(delta):
	var enemy_found = false
	if can_attack:
		if current_target != null && is_instance_valid(current_target) && !current_target.is_queued_for_deletion():
			#print("current target: ", current_target.name)
			if has_node(current_target.get_path()):
				pass
				#print("node found")
			#	$AttackTimer.start()
		else:
			bodies = $DamageArea.get_overlapping_bodies()
			for body in bodies:
				if !enemy_found:
					if ("type" in body):
						if body.type == "enemy":
							#print(body.name, "found")
							current_target = body
							enemy_found = true
			if !enemy_found:
				var areas = $DamageArea.get_overlapping_areas()
				for area in areas:
					if !enemy_found:
						var parent = area.get_parent()
						if ("type" in parent):
							if parent.type == "portal":
								print(parent.name, "found")
								current_target = parent
								enemy_found = true
					

func set_powered(value):
	powered = value
	if powered:
		$"Sprite".texture = powered_texture
	else:
		$"Sprite".texture = unpowered_texture

	emit_signal('power_changed', powered)

func turn_on():
	set_powered(true)

func turn_off():
	set_powered(false)

func _ready():
	#$DamageArea.connect("area_entered", self, "attack")
	if can_attack:
		$AttackTimer.connect("timeout", self, "attack")
		#$AttackTimer.start()
		var damage_shape = CircleShape2D.new()
		damage_shape.set_radius(damage_radius)
		$DamageArea/CollisionShape2D.shape = damage_shape

func take_damage(amt):
	health -= amt
	$Lifebar.change_value(health, max_health)
	if health <= 0:
		death()

func death():
	$Anim.play("destroyed")
	#TODO Delete after animation or spawn death carcas
	delete()

func attack():
	#print("Tower attacking")
	if can_attack:
		if current_target != null && is_instance_valid(current_target) && !current_target.is_queued_for_deletion() && has_node(current_target.get_path()):
			if $DamageArea.overlaps_body(current_target) || $DamageArea.overlaps_area(current_target.get_node("Area2D")):
				current_target.take_damage(damage)
				#print(current_target)
			else:
				current_target = null
	## 	check if type is enemy then fire at enemy
	##	set attack timer
	##	$AttackTimer.start()
	pass

# Should be used instead direct godot methods to trigger moved signal
func set_position(value):
	position = value
	emit_signal('moved', position)

func delete():
	emit_signal('about_to_be_deleted')
	queue_free()



func _on_mouse_entered():
	emit_signal("mouse_enter")


func _on_mouse_exited():
	emit_signal("mouse_leave")
