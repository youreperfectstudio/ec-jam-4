extends ElectricNode
class_name Bunker

func death():
	emit_signal("about_to_be_deleted")
	$Sprite.visible = false
	$CPUParticles2D.emitting = true